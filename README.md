# WhereWereWe
Where were we is a portfolio project consists of Vue.js and Node.js files. You can create notes and comments on individual words in those notes, which creates a social reading experience.

Tech used on backend:

 - Socket.io for Notification System
 - Mongoose for models and database operations
 - Express.js (MVC) for API
 - Restify for REST API of Users microservice
 - Event Emitter for communication between processes
 - Json web token for authentication
 - Passport for authentication
 - Joi for validating register form
 - Jade for unit testing

Tech used on frontend:

 - Vue.js 2
 - Vuex
 - Vue-router
 - Vue-cli
 - Persisted State
 - Webpack
 - Bootstrap 4
 - Socket.io-client

## How to run

You can start the individual services at same time by first going into `users` directory and running `npm run start-dev`. This will start the users service. Then you can go into `notes` directory and run `npm run start-mongo`. This will run notes service with mongoose models. Alternative models like sequelize can be added and used later (See config file for that). Lastly you can open the `client` directory and run `npm start`. After all of that, you can access to system on `localhost:8080`.

## How can I contribute ?
You can send me your code reviews or pull requests. Much appreciated.

## Licence
MIT. You can use it with any intention and purpose. I also wanted it to be an example project for Express.js MVC which I couldn't find a good one.
