const usersModel = require('../models/users-rest')
module.exports = async (io) => {
  try {
    // Get list of all users
    const response = await usersModel.list()
    // Open a namespace for each registered user
    for (const user of response) {
      io.of('/' + user._id)
    }
  } catch (err) {
    console.log(err.stack)
  }
}