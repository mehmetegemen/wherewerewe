const usersModel = require('../models/users-rest')

module.exports = {
  async updateUser(req, res){
    try {
      // Update user by form input
      let user = await usersModel.update({
        username: req.body.username,
        password: req.body.password,
        fullName: req.body.fullName,
        dateOfBirth: new Date(req.body.dateOfBirth),
        profilePicture: req.body.profilePicture,
        twitter: req.body.twitter,
        facebook: req.body.facebook,
        email: req.body.email
      });
      // Send success response
      res.send({success: true, message: "Update successful"});
    } catch (err) {
      error(`User update error : ${err.stack}`);
      res.send({success: false, message: "User update error"})
    }
  },
  testGate(req, res) {
    res.send({message: 'Authenticated'});
  },
  socketio(io) {
    let emitCreateUser = (obj) => {
      // Open a namespace for new user
      io.of('/' + obj.user._id)
    }
    // Listen for user creation event
    usersModel.events.on('usercreated', emitCreateUser)
  }
}