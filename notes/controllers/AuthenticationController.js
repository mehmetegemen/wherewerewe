const usersModel = require('../models/users-rest')
const jwt = require('jsonwebtoken')
const config = require('../config/config')

function jwtSignUser(user) {
  const ONE_YEAR = 60 * 60 * 24 * 365
  return jwt.sign(user, config.authentication.jwtSecret, {
    expiresIn: ONE_YEAR
  })
}

module.exports = {
  async register(req, res) {
    try {
      // Pass req.body properties to create an object
      let user = await usersModel.create({
        username: req.body.username,
        password: req.body.password,
        fullName: req.body.fullName,
        dateOfBirth: new Date(req.body.dateOfBirth),
        email: req.body.email
      });
      // Send success response
      res.send({success: true, message: 'Registration successful'});
    } catch (err) {
      error(`Registration error : ${err.stack}`);
      res.send({success: false, message: "Registration error"})
    }
  },
  async login(req, res) {
    try {
      // Get username and password variables from corresponding properties
      const {username, password} = req.body
      // Find user in database
      const user = await usersModel.find(username)

      if (!user) {
        // User is not found
        // Exit with failure
        return res.status(403).send({success: false, message: 'The login information was incorrect'})
      }

      // Check if password is valid
      const isPasswordValid = await usersModel.userPasswordCheck(username, password)
      if (!isPasswordValid.check) {
        // Password is invalid
        // Exit with failure
        return res.status(403).send({
          success: false,
          message: 'The login information was incorrect'
        })
      }

      // Send User object and Token
      res.send({
        user,
        token: jwtSignUser(user)
      })
    } catch (err) {
      error(`Registration error : ${err.stack}`)
      res.status(500).send({
        success: false,
        message: 'Login error on server'
      })
    }
  }
}