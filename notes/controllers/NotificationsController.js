const config = require('../config/config')
const notificationModel = require('../models/Notification')

module.exports = {
  async read(req, res){
    try {
      // Mark notification as read
      const response = await notificationModel.read(req.params._id)
      // Send notification model object as response
      res.send(response)
    } catch (err) {
      res.send({
        success: false,
        message: 'Couldnt read the notification'
      })
    }
  },
  async readMany(req, res) {
    try {
      // Read more than one notification
      // Uses read function
      await notificationModel.readMany(req.params.ids)
      // Send success response
      res.send({
        success: true
      })
    } catch (err) {
      res.send({
        success: false,
        message: 'Couldnt read many'
      })
    }
  }
}