const config = require('../config/config')
const commentsModel = require('../models/' + config.commentsModel)

module.exports = {
  async find(req, res) {
    try {
      // Find comment in database by id
      let doc = await commentsModel.find(req.params.id);
      // Send comment as response
      // Can be an object or null
      res.send(doc);
    } catch (err) {
      error(`Comment find error : ${err.stack}`)
      res.status(500).send({
        success: false,
        message: 'Comment find error on server'
      })
    }
  },
  async listByAuthor(req, res) {
    try {
      // List comments by author ids
      let docs = await commentsModel.listByAuthor(req.params.author);
      // Send comments as an array of objects
      // Can be a filled or empty array
      res.send(docs);
    } catch (err) {
      error(`Comment list by Author error : ${err.stack}`)
      res.status(500).send({
        success: false,
        message: 'Comment List by author error on server'
      })
    }
  },
  async listByPostId (req, res) {
    try {
      // List comments by post id
      let docs = await commentsModel.listByPostId(req.params.id);
      // Send comments as an array of objects
      // Can be a filled or empty array
      res.send(docs);
    } catch (err) {
      error(`Comment list by PostID error : ${err.stack}`)
      res.status(500).send({
        success: false,
        message: 'Comment List by PostID error on server'
      })
    }
  },
  async destroy (req, res) {
    try {
      // Destroy comment by id
      await commentsModel.destroy(req.params.id);
      // Inform that deletion is successful with a response
      res.send({success: true});
    } catch (err) {
      error(`Comment destroy error : ${err.stack}`)
      res.status(500).send({
        success: false,
        message: 'Comment destroy error on server'
      })
    }
  },
  async create (req, res){
    try {
      // Pass req.body properties to create an object
      let doc = await commentsModel.create({
        postId: req.body.postId,
        author: req.body.author,
        comment: req.body.comment,
        where: req.body.where
      });
      // Send model object as a response
      res.send(doc);
    } catch (err) {
      error(`Comment create error : ${err.stack}`)
      res.status(500).send({
        success: false,
        message: 'Comment create error on server'
      })
    }
  },
  async update (req, res) {
    try {
      // Update comment by id
      let doc = await commentsModel.update({
        id: req.body._id,
        comment: req.body.comment,
        where: req.body.where
      });
      // Send model object as a response
      res.send(doc);
    } catch (err) {
      error(`Comment update error : ${err.stack}`)
      res.status(500).send({
        success: false,
        message: 'Comment update error on server'
      })
    }
  },
  socketio(io){
    let emitCreateComment = (obj) => {
      // Comment created
      // Emit a message on namespace of user id
      io.of('/'+obj.user).emit('notification', obj.comment)
    }
    // Listen for comment creation
    commentsModel.events.on('commentcreated', emitCreateComment)
  }
}