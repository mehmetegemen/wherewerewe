const config = require('../config/config')
const notesModel = require('../models/' + config.notesModel)
const error = require('debug')('notes:error')
const log = require('debug')('notes:controller')
module.exports = {
  async findById(req, res) {
    try {
      // Find note by id
      let doc = await notesModel.findByID(req.params.id);
      // Returns the found model object
      res.send(doc);
    } catch (err) {
      error(`Note find by id error : ${err.stack}`)
      res.status(500).send({
        success: false,
        message: 'Note find by id error on server'
      })
    }
  },
  async findByTitle(req, res) {
    try {
      // Find note by Title
      // Useful for search functionality
      let doc = await notesModel.findByTitle(req.params.title);
      // Returns the found model object
      res.send(doc);
    } catch (err) {
      error(`Note find by title error : ${err.stack}`)
      res.status(500).send({
        success: false,
        message: 'Note find by title error on server'
      })
    }
  },
  async listByAuthor(req, res) {
    try {
      // List notes by author id
      let list = await notesModel.listByAuthor(req.params.author);
      // Send list array
      // May be empty or filled
      res.send(list);
    } catch (err) {
      error(`Note list by author error : ${err.stack}`)
      res.status(500).send({
        success: false,
        message: 'Note list by author error on server'
      })
    }
  },
  async destroy (req, res) {
    try {
      // Delete note by id
      await notesModel.destroy(req.params.id);
      // Inform user that process is successful
      res.send({success: true});
    } catch (err) {
      error(`Note destroy error : ${err.stack}`)
      res.status(500).send({
        success: false,
        message: 'Note destroy error on server'
      })
    }
  },
  async create (req, res) {
    try {
      // Create note by form input
      let doc = await notesModel.create({
        title: req.body.title,
        author: req.user._id,
        text: req.body.text
      });
      // Send note model object
      res.send(doc);
    } catch (err) {
      error(`Note create error : ${err.stack}`)
      res.status(500).send({
        success: false,
        message: 'Note create error on server'
      })
    }
  },
  async update (req, res) {
    try {
      // Update note by form input
      let doc = await notesModel.update({
        id: req.body.id,
        title: req.body.title,
        author: req.user._id,
        text: req.body.text
      });
      // Send note model object
      res.send(doc);
    } catch (err) {
      error(`Note update error : ${err.stack}`)
      res.status(500).send({
        success: false,
        message: 'Note update error on server'
      })
    }
  },
  async getNotes (req, res) {
    try {
      // Get notes by specific skip and limit
      // For homepage
      let docs = await notesModel.getNotes(req.params.skip, req.params.limit);
      res.send(docs);
    } catch (err) {
      error(`getNotes error : ${err.stack}`);
      res.status(500).send({
        success: false,
        message: 'Couldnt retrieve notes'
      })
    }
  }
}
