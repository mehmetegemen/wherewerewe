const isAuthenticated = require('./policies/isAuthenticated')
const AuthenticationControllerPolicy = require('./policies/AuthenticationControllerPolicy')
const AuthenticationController = require('./controllers/AuthenticationController')
const UsersController = require('./controllers/UsersController')
const NotesController = require('./controllers/NotesController')
const CommentsController = require('./controllers/CommentsController')
const NotificationsController = require('./controllers/NotificationsController')

const rateLimit = require("express-rate-limit")

const crudRateLimit = rateLimit(
  {
    windowMs: 1000 * 60 * 60 * 24,
    max: 100
  }
)

const authRateLimit = rateLimit(
  {
    windowsMs: 1000 * 60 * 60 * 24,
    max:20
  }
)

const registerRateLimit = rateLimit(
  {
    windowsMs: 1000 * 60 * 60 * 24,
    max:5
  }
)

module.exports = (app, io) => {
  app.post('/login', authRateLimit, AuthenticationController.login)
  app.post('/register', registerRateLimit, AuthenticationControllerPolicy.register, AuthenticationController.register)

  app.get('/test-gate', isAuthenticated, UsersController.testGate)
  UsersController.socketio(io)

  app.post('/notes/create', isAuthenticated, crudRateLimit, NotesController.create)
  app.post('/notes/update', isAuthenticated, crudRateLimit, NotesController.update)
  app.get('/notes/findById/:id', NotesController.findById)
  app.get('/notes/findByTitle/:title', NotesController.findByTitle)
  app.get('/notes/listByAuthor/:author', NotesController.listByAuthor)
  app.get('/notes/destroy/:id', isAuthenticated, NotesController.destroy)
  app.get('/notes/getNotes/:skip/:limit', NotesController.getNotes)

  app.post('/notifications/read', isAuthenticated, NotificationsController.read)
  app.post('/notifications/readMany', isAuthenticated, NotificationsController.readMany)

  app.get('/comments/find/:id', CommentsController.find)
  app.get('/comments/listByAuthor/:author', CommentsController.listByAuthor)
  app.get('/comments/listByPostId/:id', CommentsController.listByPostId)
  app.post('/comments/update', isAuthenticated, crudRateLimit, CommentsController.update)
  app.post('/comments/create', isAuthenticated, crudRateLimit, CommentsController.create)
  app.get('/comments/destroy/:id', isAuthenticated, CommentsController.destroy)
  CommentsController.socketio(io)
}