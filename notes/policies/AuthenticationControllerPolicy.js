const BaseJoi = require('Joi')
const JoiDateExtension = require('joi-date-extensions')
const Joi = BaseJoi.extend(JoiDateExtension)
module.exports = {
  register (req, res, next) {
    // Create Joi schema to validate form input
    const schema = {
      // Email validation comes from an extension
      email: Joi.string().email(),
      // Check user if it consists of
      // words, "-", "_" and digits
      username: Joi.string().regex(
        /^[\w\-_\d]+$/
      ),
      // Check that password has
      // ONE upper case letter, ONE lower case letter,
      // ONE digit and ONE special character
      password: Joi.string().regex(
        new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$')
      ),
      // Check that full name is more than 3 letters
      fullName: Joi.string().regex(
        /^[a-zA-Z ]{3,}$/
      ),
      dateOfBirth: Joi.date()
    }

    const {error} = Joi.validate(req.body, schema)
    if (error) {
      switch (error.details[0].context.key) {
        case 'email':
          res.status(400).send(
            {
              success: false,
              message: 'E-mail is not valid'
            }
          )
          break

        case 'password':
          res.status(400).send(
            {
              success: false,
              message: 'Password must have at least ONE upper case letter, ONE lower case letter, ONE digit and ONE special character. It must be at least 8 characters long.'
            }
          )
          break
        case 'dateOfBirth':
          res.status(400).send(
            {
              success: false,
              message: 'Date of birth is not valid.'
            }
          )
          break
        case 'username':
          res.status(400).send(
            {
              success: false,
              message: 'Username is not valid.'
            }
          )
          break
        case 'fullName':
          res.status(400).send(
            {
              success: false,
              message: 'Full name is not valid.'
            }
          )
          break
        default:
          res.status(400).send({
            success: false,
            message: 'Form is not valid'
          })
      }
    } else {
      next()
    }

  }
}