const path = require('path')

module.exports = {
  "notePort": process.env.PORT || 3000,
  "mongoUrl": process.env.MONGO_URL || "mongodb://localhost:27017/wherewerewe",
  "userServiceUrl": process.env.USER_SERVICE_URL || "http://localhost:3333",
  "notesModel": process.env.NOTES_MODEL || "notes-mongo",
  "commentsModel": process.env.COMMENTS_MODEL || "comments-mongo",
  authentication: {
    jwtSecret: process.env.JWT_SECRET || 'black car'
  }
}