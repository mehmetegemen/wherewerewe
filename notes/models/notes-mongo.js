const util = require('util');
const Note = require('./Note');
const log = require('debug')('notes:mongo-model');
const error = require('debug')('notes:error');
const mongoose = require('mongoose');

let db;

exports.connectDB = function () {
  return new Promise((resolve, reject) => {
    if (db) return resolve(db);
    mongoose.connect(process.env.MONGO_URL);
    db = mongoose.connection;
    resolve(mongoose.connection);
  });
};


exports.create = async function (details) {
  try {
    // Connect to database
    await exports.connectDB();
    return new Promise((resolve, reject) => {
      // Create note by form input
      Note.create({
        title: details.title,
        author: details.author,
        text: details.text
      }, (err, model) => {
        if (err)return reject(err);
        resolve(model);
      })
    });
  } catch (err) {
    error(`Note Create error : ${err.stack}`);
  }
};

exports.update = async function (details) {
  try {
    // Connect to database
    await exports.connectDB();
    return new Promise((resolve, reject) => {
      // Find and update note by id
      Note.findOneAndUpdate({_id: details.id}, {
        $set: {
          title: details.title,
          author: details.author,
          text: details.text,
          updatedDate: Date.now()
        }
      }, {"new": true}, function (err, doc) {
        if (err) return reject(err);
        log(`Note model updated : ${util.inspect(doc)}`);
        resolve(doc);
      })
    });
  } catch (err) {
    error(`Note update error : ${err.stack}`);
  }
};

exports.findByID = async function (id) {
  try {
    // Connect to database
    await exports.connectDB();
    return new Promise((resolve, reject) => {
      // Find note by id
      Note.findOne({_id: id}, function (err, doc) {
        if (err) return reject(err);
        log(`Note found : ${util.inspect(doc)}`);
        resolve(doc);
      });
    });
  } catch (err) {
    error(`Note find(id) error : ${err.stack}`);
  }
};

exports.findByTitle = async function (title) {
  try {
    // Connect to database
    await exports.connectDB();
    return new Promise((resolve, reject) => {
      // Find note by title
      Note.find({title}, function (err, docs) {
        if (err) return reject(err);
        log(`Notes found : ${util.inspect(docs)}`);
        // Resolve an array
        resolve(docs);
      });
    });
  } catch (err) {
    error(`Note find(title) error : ${err.stack}`);
  }
};

exports.destroy = async function (id) {
  try {
    // Connect to database
    await exports.connectDB();
    return new Promise((resolve, reject) => {
      // Find and delete note by id
      Note.deleteOne({_id: id}, function (err) {
        if (err) return reject(err);
        log(`Note destroyed ${id}`);
        resolve();
      });
    });
  } catch (err) {
    error(`Note destroy error : ${err.stack}`);
  }
};

exports.listByAuthor = async function (author) {
  try {
    // Connect to database
    await exports.connectDB();
    return new Promise((resolve, reject) => {
      // Find notes by author id
      Note.find({author}, function (err, docs) {
        if (err) return reject(err);
        // Resolve notes array
        resolve(docs);
      });
    });
  } catch (err) {
    error(`Note list error : ${err.stack}`);
  }
};

exports.count = async function () {
  try {
    // Connect to database
    await exports.connectDB();
    return new Promise((resolve, reject) => {
      // Count the amount of notes in database
      Note.count({}, function (err, count) {
        if (err) return reject(err);
        resolve(count);
      });
    });
  } catch (err) {
    error(`Note count error : ${err.stack}`);
  }
};

exports.getNotes = async function (skip = 10, limit = 10) {
  try {
    // Limit skip and limit parameters
    skip = Number(skip); // req.param return string
    limit = Number(limit);
    if (limit > 20) limit = 20; // Max limit limit
    if (limit < 1) limit = 0; // Lowest limit limit
    if (skip > 2147483645) skip = 2147483645; // Max skip limit by max 32-bit NUMBER
    if (skip < 1) skip = 0; // Lowest skip limit
    // Connect to database
    await exports.connectDB();
    return new Promise((resolve, reject) => {
      // Retrieve all notes by limit and skip
      Note.find({}).skip(skip).limit(limit).exec(function (err, docs) {
        if (err) return reject(err);
        resolve(docs);
      })
    })
  } catch (err) {
    error(`getNotes error : ${err.stack}`)
  }
}