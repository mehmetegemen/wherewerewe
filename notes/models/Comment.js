const mongoose = require('mongoose');

let Comment = mongoose.model('Comment', {
  postId: String,
  author: String,
  comment: String,
  where: {type: Number, default: 0},
  createdDate: {type: Date, default: Date.now},
  updatedDate: {type: Date, default: Date.now}
});

module.exports = Comment;