const mongoose = require('mongoose');

let Note = mongoose.model('Note', {
  title: String,
  createdDate: {type: Date, default: Date.now},
  updatedDate: {type: Date, default: Date.now},
  author: String,
  rating: {type: Number, default: 0},
  hit: {type: Number, default: 0},
  text: String
});

module.exports = Note;