const mongoose = require('mongoose')

let Notification = mongoose.model('Notification', {
  userID: String,
  targetID: String,
  message: String,
  read: {type: Number, default: 0},
  createdDate: {type: Date, default: Date.now}
})

module.exports = Notification