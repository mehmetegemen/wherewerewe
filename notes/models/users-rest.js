'use strict';
const restify = require('restify-clients');
const log = require('debug')('notes:users-rest-client');
const error = require('debug')('notes:error');

// Connect to Restify server by JSON client
let connectRest = function () {
  return new Promise((resolve, reject) => {
    try {
      resolve(restify.createJsonClient({
        url: process.env.USER_SERVICE_URL,
        version: "*"
      }));
    } catch (err) {

    }
  }).then(client => {
    // Server requires credentials
    // Login to server
    client.basicAuth('them', 'D4ED43C0-8BD6-4FE2-B358-7C0E230D11EF');
    return client;
  });
};

exports.create = async function (details) {
  try {
    // Connect with restify client
    let client = await connectRest();
    return new Promise((resolve, reject) => {
      client.post('/create-user', {
          username: details.username,
          password: details.password,
          fullName: details.fullName,
          dateOfBirth: new Date(details.dateOfBirth),
          email: details.email
        },
        (err, req, res, obj) => {
          if (err) return reject(err);
          exports.events.userCreated(obj)
          resolve(obj);
        }
      );
    });
  } catch (err) {
    log(`Rest create error : ${err.stack}`);
  }
};

exports.update = async function (details) {
  try {
    // Connect with restify client
    let client = await connectRest();
    return new Promise((resolve, reject) => {
      client.post('/update-user', {
          username: details.username,
          password: details.password,
          fullName: details.fullName,
          dateOfBirth: details.dateOfBirth,
          profilePicture: details.profilePicture,
          twitter: details.twitter,
          facebook: details.facebook,
          email: details.email
        },
        (err, req, res, obj) => {
          if (err) return reject(err);
          resolve(obj);
        }
      );
    });
  } catch (err) {
    log(`Rest update error : ${err.stack}`);

  }
};

exports.find = async function (username) {
  try {
    // Connect with restify client
    let client = await connectRest();
    return new Promise((resolve, reject) => {
      client.get('/find/' + username, (err, req, res, obj) => {
        if (err) return reject(err);
        resolve(obj);
      });
    });
  } catch (err) {

    log(`Rest find error : ${err.stack}`);
  }
};

exports.userPasswordCheck = async function (username, password) {
  try {
    // Connect with restify client
    let client = await connectRest();
    return new Promise((resolve, reject) => {
      client.post('/passwordCheck', {
        username,
        password
      }, (err, req, res, obj) => {
        if (err) return reject(err);
        resolve(obj);
      });
    });
  } catch (err) {

    log(`Rest passwordCheck error : ${err.stack}`);
  }
};

exports.destroy = async function (username) {
  try {
    // Connect with restify client
    let client = await connectRest();
    return new Promise((resolve, reject) => {
      client.get('/destroy/' + username, (err, req, res, obj) => {
        if (err)return reject(err);
        resolve();
      });
    });
  } catch (err) {

    log(`Rest destroy error : ${err.stack}`);
  }
};

exports.list = async function () {
  try {
    // Connect with restify client
    let client = await connectRest();
    return new Promise((resolve, reject) => {
      client.get('/list', (err, req, res, obj) => {
        if (err) return reject(err);
        resolve(obj);
      });
    });
  } catch (err) {
    log(`Rest list error : ${err.stack}`);
  }
};

// Import event emitter of user events
exports.events = require('./users-events')