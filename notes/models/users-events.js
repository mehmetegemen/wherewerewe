const EventEmitter = require('events')
class UsersEmitter extends EventEmitter {}

module.exports = new UsersEmitter()

module.exports.userCreated = function (user) {
  // Emit user creation event for
  // listening event in controller
  module.exports.emit('usercreated', {
    user
  })
}