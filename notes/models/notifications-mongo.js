const util = require('util')
const Notification = require('Notification')
const log = require('debug')('notifications:mongo-model')
const error = require('debug')('notes:error')
const mongoose = require('mongoose')

let db;

exports.connectDB = function () {
  return new Promise((resolve, reject) => {
    if (db) return resolve(db);
    mongoose.connect(process.env.MONGO_URL)
    db = mongoose.connection
    resolve(mongoose.connection)
  })
}

exports.create = async function (userID, targetID, message) {
  try {
    // Connect to database
    await exports.connectDB();
    return new Promise((resolve, reject) => {
      // Create a notification in database
      Notification.create({userID, targetID, message}, (err, model) => {
        if (err) return reject(err)
        resolve(model)
      })
    })
  } catch (err) {
    error(`Notification create error : ${err.stack}`)
  }
}

exports.read = async function (_id) {
  try {
    // Connect to database
    await exports.connectDB()
    return new Promise((resolve, reject) => {
      // Find and update notifications status in database
      Notification.findOneAndUpdate({_id}, {
        $set: {
          read: 1
        }
      }, {"new": true}, function (err, doc) {
        if (err) return reject(err)
        resolve(doc)
      })
    })
  } catch (err) {
    error(`Notification read error : ${err.stack}`)
  }
}

exports.readMany = async function(ids) {
  try {
    // Read more than one notification
    // Uses "read" function
    for (id of ids) {
      await exports.read(id)
    }
  } catch (err) {
    error(`Notification readMany error : ${err.stack}`)
  }
}