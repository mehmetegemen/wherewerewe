const util = require('util');
const log = require('debug')('notes:comments-mongo');
const error = require('debug')('notes:error');
const Comment = require('./Comment');
const mongoose = require('mongoose');
const notesModel = require('./notes-mongo')

let db;

exports.connectDB = function () {
  return new Promise((resolve, reject) => {
    if (db) return resolve(db);
    mongoose.connect(process.env.MONGO_URL);
    db = mongoose.connection;
    resolve(mongoose.connection);
  });
};

exports.create = async function (details) {
  try {
    // Connect to database
    await exports.connectDB();
    // Find comment's author id by post
    const user = (await notesModel.findByID(details.postId)).author
    return new Promise((resolve, reject) => {
      // Create comment by form input
      Comment.create({
        postId: details.postId,
        author: details.author,
        comment: details.comment,
        where: details.where
      }, function (err, doc) {
        if (err) return reject(err);
        log(`Comment created : ${util.inspect(doc)}`);
        exports.events.commentCreated(doc, user)
        resolve(doc);
      });
    });
  } catch (err) {
    error(`Comment create error : ${err}`);
  }
};

exports.update = async function (details) {
  try {
    // Connect to database
    await exports.connectDB();
    return new Promise((resolve, reject) => {
      // Find and update comment by id
      Comment.findOneAndUpdate({_id: details.id}, {$set: {
        comment: details.comment,
        where: details.where
      }}, {"new": true}, function (err, doc) {
        if (err) return reject(err);
        log(`Comment updated : ${util.inspect(doc)}`);
        resolve(doc);
      })
    });
  } catch (err) {
    error(`Comment update error : ${err}`);
  }
};

exports.destroy = async function (_id) {
  try {
    // Connect to database
    await exports.connectDB();
    return new Promise((resolve, reject) => {
      // Delete comment by id
      Comment.deleteOne({_id}, function (err) {
        if (err) return reject(err);
        resolve();
      });
    });
  } catch (err) {
    error(`Comment delete error : ${err}`);
  }
};

exports.find = async function (_id) {
  try {
    // Connect to database
    await exports.connectDB();
    return new Promise((resolve, reject) => {
      // Find comment by id
      Comment.findOne({_id}, function (err, doc) {
        if (err) return reject(err);
        log(`Comment found : ${util.inspect(doc)}`);
        resolve(doc);
      })
    });
  } catch (err) {
    error(`Comment find error : ${err}`);
  }
};

exports.listByPostId = async function (postId) {
  try {
    // Connect to database
    await exports.connectDB();
    return new Promise((resolve, reject) => {
      // Find comment by entered post's id
      Comment.find({postId}, function (err, docs) {
        if (err) return reject(err);
        log(`Comments listed by postID : ${util.inspect(docs)}`);
        resolve(docs);
      });
    });
  } catch (err) {
    error(`Comment listByPostID error : ${err}`);
  }
};

exports.listByAuthor = async function (author) {
  try {
    // Connect to database
    await exports.connectDB();
    return new Promise((resolve, reject) => {
      // Create an empty array to fill with comment ids
      // by author id
      let _ids = [];
      // Find comments
      Comment.find({author}, function (err, docs) {
        if (err) return reject(err);
        log(`Comments listed by author : ${util.inspect(docs)}`);
        // Fill _ids array with comments
        for (let doc of docs) {
          _ids.push(doc._id);
        }
        resolve(_ids);
      })
    });
  } catch (err) {
    error(`Comment listByAuthor error : ${err}`);
  }
};

// Import event emitter of comment events
exports.events = require('./comments-events')