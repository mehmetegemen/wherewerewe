const EventEmitter = require('events')
class CommentsEmitter extends EventEmitter {}

module.exports = new CommentsEmitter()

module.exports.commentCreated = function (comment, user) {
  // Emit comment creation event for
  // listening event in controller
  module.exports.emit('commentcreated', {
    comment,
    user
  })
}