const commentsMongo = require('../models/comments-mongo');

test(`Test to create comment`, async () => {
  let comment = await commentsMongo.create('postid0', 'test author', 'test comment', 65);
  expect(comment.postID).toBe('postid0');
  expect(comment.author).toBe('test author');
  expect(comment.comment).toBe('test comment');
  expect(comment.where).toBe(65);
});

test(`Test to listByAuthor and update comment`, async () => {
  let docIDs = await commentsMongo.listByAuthor('test author');
  let updatedDoc = await commentsMongo.update(docIDs[0], 'updated test comment', 25);
  expect(updatedDoc.comment).toBe('updated test comment');
  expect(updatedDoc.where).toBe(25);
});

test(`Test to listByPostID`, async () => {
  let docs = await commentsMongo.listByPostID('postid0');
  expect(docs).not.toBeNull();
});

test(`Test to find comment`, async () => {
  let docs = await commentsMongo.listByPostID('postid0');
  let docID = docs[0];
  let doc = await commentsMongo.find(docID);
  expect(doc).not.toBeNull();
});

test(`Test to destroy comment`, async () => {
  let docs = await commentsMongo.listByPostID('postid0');
  let docID = docs[0];
  await commentsMongo.destroy(docID);
  let doc = await commentsMongo.find(docID);
  expect(doc).toBeNull();
});