const Note = require('../models/Note');
const notesMongo = require('../models/notes-mongo');

test(`Test to create note`, async () => {
  let result = await notesMongo.create('test title', 'test author', 'test text');
  expect(result.title).toBe("test title");
  expect(result.author).toBe("test author");
  expect(result.text).toBe("test text");
});

test(`Test to find note by title`, async () => {
  let result = await notesMongo.findByTitle('test title');
  expect(result.title).toBe('test title');
  expect(result.author).toBe('test author');
  expect(result.text).toBe('test text');
});

test(`Test to update note`, async () => {
  let doc = await notesMongo.findByTitle('test title');
  let updatedDoc = await notesMongo.update(doc._id, 'updated test title', 'updated test author', 'updated test text');
  expect(updatedDoc.title).toBe('updated test title');
  expect(updatedDoc.author).toBe('updated test author');
  expect(updatedDoc.text).toBe('updated test text');
});

test(`Test to list by author`, async () => {
  let docs = await notesMongo.listByAuthor('updated test author');
  expect(docs).not.toBeNull();
});

test(`Test to count notes`, async () => {
  let count = await notesMongo.count();
  expect(count).toBeGreaterThan(0);
});


test(`Test to destroy note and find by ID`, async () => {
  let doc = await notesMongo.findByTitle('updated test title');
  await notesMongo.destroy(doc._id);
  let destroyedDoc = await notesMongo.findByID(doc._id);
  expect(destroyedDoc).toBeNull();
});
