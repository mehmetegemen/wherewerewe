const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt
const config = require('./config/config')
const usersRest = require('./models/users-rest')

module.exports.authorize = () => {
  const strategy = new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromUrlQueryParameter('token'),
    secretOrKey: config.authentication.jwtSecret
  }, async function (jwtPayload, done) {
    try {
      const user = await usersRest.find(jwtPayload.username)
      if (!user) {
        return done(new Error(), false)
      }
      return done(null, user)
    } catch (err) {
      return done(new Error(), false)
    }
  });

  return function authorize(socket, accept) {
    strategy.success = function success(user) {
      socket.handshake.user = user;
      accept()
    };

    strategy.fail = info => accept(new Error(info));

    strategy.error = error => accept(error);

    strategy.authenticate(socket.request, {})
  }
};