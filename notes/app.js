#!/user/bin/env node
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var logger = require('morgan');
var debug = require('debug')('notes:server');
//var https = require('https');
var fs = require('fs');
const cors = require('cors')
const config = require('./config/config')
var app = express();
const http = require('http')
const server = http.createServer(app)
const io = require('socket.io')(server)
const passportJwtSocketIo = require('./passport-jwt.socketio')
var PORT = process.env.PORT || 8443;
var HOST = process.env.HOST || '';
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});
app.use(cors());
// catch 404 and forward to error handler
/*app.use(function(req, res, next) {
 next(createError(404));
 });*/

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send(err.stack);
});

io.use(passportJwtSocketIo.authorize());

require('./passport');
require('./routes')(app,io);
require('./Helpers/initializeSockets')(io)

/*
 app.use (function (req, res, next) {
 if (req.secure) {
 next();
 } else {
 res.redirect('https://' + req.headers.host + req.url);
 }
 });

 var options = {
 key:fs.readFileSync('ssl/key.pem'),
 ca:fs.readFileSync('ssl/csr.pem'),
 cert:fs.readFileSync('ssl/cert.pem'),
 };

 https.createServer(options, app).listen(PORT, HOST, null, function() {
 console.log('Server listening on port %d in %s mode', this.address().port, app.settings.env);
 });
 */

//app.listen(config.notePort || 3000);
server.listen(config.notePort || 3000);

module.exports = app;
