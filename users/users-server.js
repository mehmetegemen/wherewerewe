const util = require('util');
const log = require('debug')('users:users-server');
const error = require('debug')('users:error');
const restify = require('restify');
const usersModel = require('./models/users-mongo');

let server = restify.createServer({
  name: "Users-Auth-Service",
  version: "0.0.1"
});


server.use(restify.plugins.authorizationParser());
server.use(check); // Check for credentials of restify clients
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser({
  mapParams: true
}));

server.post('/create-user', async function (req, res, next) {
  try {
    // TODO: pass an object
    let doc = await usersModel.create(req.body.username, req.body.password, req.body.fullName, req.body.dateOfBirth, req.body.email);
    log(`User created ${util.inspect(doc)}`);
    // Send user model object as response
    res.send(doc);
    next(false);
  } catch (err) {
    error(`User could not created : ${err}`);
  }
});

server.post('/update-user', async function (req, res, next) {
  try {
    let doc = await usersModel.update(req.body.username, req.body.password, req.body.fullName, req.body.dateOfBirth, req.body.profilePicture, req.body.twitter, req.body.facebook, req.body.email);
    log(`User updated ${util.inspect(doc)}`);
    // Send user model object as response
    res.send(doc);
    next(false);
  } catch (err) {
    error(`User could not updated : ${err}`)
  }
});

server.get('/find/:username', async function (req, res, next) {
  try {
    let doc = await usersModel.find(req.params.username);
    // Send user model object as response
    res.send(doc);
    next(false);
  } catch (err) {
    error(`User could not find : ${util.inspect(doc)}`);
  }
});

server.get('/destroy/:username', async function (req, res, next) {
  try {
    await usersModel.destroy(req.params.username);
    log(`User destroyed`);
    res.send(
      {
        success: true,
        message: 'User is deleted'
      }
    );
    next(false);
  } catch (err) {
    error(`User could not be destroyed `);
  }
});

server.post('/passwordCheck', async function (req, res, next) {
  try {
    let check = await usersModel.userPasswordCheck(req.body.username, req.body.password);
    // Send an object with check and username properties
    res.send(check);
    next(false);
  } catch (err) {
    error(`User password check error : ${err}`);
  }
});

server.get('/list', async function (req, res, next) {
  try {
    // Get list of users
    let list = await usersModel.list();
    // If list is not defined
    // Create an empty array
    if (!list) list = [];
    // Send list array as a response
    res.send(list);
    next(false);
  } catch (err) {
    error(`User list error : ${err}`)
  }
});

server.listen(process.env.PORT, "localhost", function () {
  log(server.name + ' listening at ' + server.url);
});

// Mimic API Key authentication.
var apiKeys = [{
  user: 'them',
  key: 'D4ED43C0-8BD6-4FE2-B358-7C0E230D11EF'
}];

function check(req, res, next) {
  if (req.authorization) {
    let found = false;
    for (let auth of apiKeys) {
      if (auth.key === req.authorization.basic.password
        && auth.user === req.authorization.basic.username) {
        found = true;
        break;
      }
    }
    if (found) next();
    else {
      res.send(401, new Error("Not authenticated"));
      error('Failed authentication check '
        + util.inspect(req.authorization));
      next(false);
    }
  } else {
    res.send(500, new Error('No Authorization Key'));
    error('NO AUTHORIZATION');
    next(false);
  }
}

