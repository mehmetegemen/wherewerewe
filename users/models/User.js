const mongoose = require('mongoose');

let User = mongoose.model('User', {
  username: String,
  password: String,
  fullName: String,
  dateOfBirth: {type: Date, default: Date.now},
  profilePicture: String,
  about: String,
  twitter: String,
  facebook: String,
  email: String
});

module.exports = User;