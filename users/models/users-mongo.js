const util = require('util');
const log = require('debug')('users:users-mongo');
const error = require('debug')('users:error');
const User = require('./User');
const mongoose = require('mongoose');

let db;

exports.connectDB = function () {
  return new Promise((resolve, reject) => {
    if (db) return resolve(db);
    mongoose.connect(process.env.USERS_MONGO_URL);
    db = mongoose.connection;
    resolve(mongoose.connection);
  });
};

exports.create = async function (username, password, fullName, dateOfBirth, email) {
  try {
    // Connect to database
    await exports.connectDB();
    return new Promise((resolve, reject) => {
      // create user by given parameters
      // TODO: make params an object
      // TODO: encrypt the password
      User.create({username, password, fullName, dateOfBirth, email}, (err, doc) => {
        if (err)return reject(err);
        resolve(doc);
      })
    });
  } catch (err) {
    error(`users create err : ${err.stack}`);

  }
};

exports.update = async function (username, password, fullName, dateOfBirth, profilePicture, twitter, facebook, email) {
  try {
    // Connect to database
    await exports.connectDB();
    return new Promise((resolve, reject) => {
      // Find user by username and update it
      User.findOneAndUpdate({username}, {
        $set: {
          password,
          fullName,
          dateOfBirth,
          profilePicture,
          twitter,
          facebook,
          email
        }
      }, (err, doc) => {
        if (err)return reject(err);
        resolve(doc);
      });
    });
  } catch (err) {
    error(`users update err : ${err.stack}`);
  }
};

exports.find = async function (username) {
  try {
    // Connect to database
    await exports.connectDB();
    return new Promise((resolve, reject) => {
      // Find user by username
      User.findOne({username}, (err, doc) => {
        if (err)return reject(err);
        resolve(doc);
      });
    });
  } catch (err) {
    error(`users find err : ${err.stack}`);

  }
};

exports.destroy = async function (username) {
  try {
    // Connect to database
    await exports.connectDB();
    return new Promise((resolve, reject) => {
      // Find user by username and delete it
      User.deleteOne({username}, (err) => {
        if (err) return reject(err);
        resolve();
      });
    });
  } catch (err) {
    error(`users destroy err : ${err.stack}`);
  }
};

exports.sanitize = function (user) {
  // Omit password from user object
  return {
    username: user.username,
    fullName: user.fullName,
    dateOfBirth: user.dateOfBirth,
    profilePicture: user.profilePicture,
    twitter: user.twitter,
    facebook: user.facebook,
    email: user.email
  };
};

exports.list = async function () {
  try {
    // Connect to database
    await exports.connectDB();
    return new Promise((resolve, reject) => {
      // Find all users
      User.find({}, (err, docs) => {
        if (err) return reject(err);
        docs.map((doc) => exports.sanitize(doc));
        // Resolve an array
        resolve(docs);
      })
    });
  } catch (err) {
    error(`users list err : ${err.stack}`);
  }
};

exports.userPasswordCheck = async function (username, password) {
  try {
    // Connect to database
    await exports.connectDB();
    let user = await exports.find(username);
    if (!user) {
      return {check: false, username, message: "User could not found"}
    } else if (username === user.username && password === user.password) {
      return {check: true, username: user.username}
    } else {
      return {check: false, username: user.username, message: "Incorrect password"}
    }
  } catch (err) {
    error(`users passwordCheck err : ${err.stack}`);
    return {check: false, username, message: "An error occured"}
  }
};