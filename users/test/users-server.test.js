const restify = require('restify-clients');

let client = restify.createJsonClient({
  url: "http://localhost:" + process.env.PORT,
  version: "*"
});

client.basicAuth('them', 'D4ED43C0-8BD6-4FE2-B358-7C0E230D11EF');

test('test /create-user', async () => {
  let result = await new Promise((resolve, reject) => {
    client.post('/create-user', {
      username: "test user",
      password: "test password",
      fullName: "test user",
      dateOfBirth: new Date('09/12/2018'),
      email: "testuser@test.com"
    }, (err, req, res, obj) => {
      resolve(obj);
    });
  });
  expect(result).not.toBeNull();
  expect(result.username).toBe('test user');
  expect(result.password).toBe('test password');
  expect(result.fullName).toBe('test user');
  expect(result.email).toBe('testuser@test.com');
});

test('test /update-user', async () => {
  let result = await new Promise((resolve, reject) => {
    client.post('/update-user', {
      username: "test user",
      password: "new password",
      fullName: "new full name",
      dateOfBirth: new Date('09/13/2018'),
      email: "newtestuser@test.com",
      twitter: "newtwitter",
      facebook: "newfacebook",
      profilePicture: "pp.png",
    }, (err, req, res, obj) => {
      resolve(obj);
    });
  });
  expect(result).not.toBeNull();
  expect(result.username).toBe('test user');
  expect(result.password).toBe('new password');
  expect(result.fullName).toBe('new full name');
  expect(result.email).toBe('newtestuser@test.com');
  expect(result.twitter).toBe('newtwitter');
  expect(result.facebook).toBe('newfacebook');
  expect(result.profilePicture).toBe('pp.png');
});

test(`test to /find/:username`, async () => {
  let result = await new Promise((resolve, reject) => {
    client.get('/find/test%20user', (err, req, res, obj) => {
      resolve(obj);
    });
  });
  expect(result).not.toBeNull();
  expect(result.username).toBe('test user');
  expect(result.password).toBe('new password');
  expect(result.fullName).toBe('new full name');
  expect(result.email).toBe('newtestuser@test.com');
  expect(result.twitter).toBe('newtwitter');
  expect(result.facebook).toBe('newfacebook');
  expect(result.profilePicture).toBe('pp.png');
});

test('test /passwordCheck', async () => {
  let result = await new Promise((resolve, reject) => {
    client.post('/passwordCheck', {
      username: "test user",
      password: "new password",
    }, (err, req, res, obj) => {
      resolve(obj);
    });
  });
  expect(result).not.toBeNull();
  expect(result.check).toBe(true);
});

test(`test /list`, async () => {
  let result = await new Promise((resolve, reject) => {
    client.get('/list', (err, req, res, obj) => {
      resolve(obj);
    });
  });
  expect(Array.isArray(result)).toBe(true);
});

test(`test /destroy/:username`, async () => {
  let result = await new Promise((resolve, reject) => {
    client.get('/destroy/test%20user', (err, req, res, obj) => {
      resolve(obj);
    });
  });
  expect(result).toBe(true);
});