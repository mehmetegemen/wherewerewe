const users_model = require('../models/users-mongo');

test(`Test to create user`, async () => {
  let doc = await users_model.create('test user', 'test password', 'test user', new Date('09/12/2018'), 'testuser@test.com');
  expect(doc).not.toBeNull();
  expect(doc.username).toBe('test user');
  expect(doc.password).toBe('test password');
  expect(doc.fullName).toBe('test user');
  expect(doc.email).toBe('testuser@test.com');
});

test(`test to update user`, async () => {
  let doc = await users_model.update('test user', 'new password', 'new full name', new Date('09/13/2018'), 'pp.png', 'newtwitter', 'newfacebook', 'newtestuser@test.com');
  expect(doc.password).toBe('new password');
  expect(doc.fullName).toBe('new full name');
  expect(doc.profilePicture).toBe('pp.png');
  expect(doc.twitter).toBe('newtwitter');
  expect(doc.facebook).toBe('newfacebook');
  expect(doc.email).toBe('newtestuser@test.com');
});

test(`test to find user`, async () => {
  let doc = await users_model.find('test user');
  expect(doc.password).toBe('new password');
  expect(doc.fullName).toBe('new full name');
  expect(doc.profilePicture).toBe('pp.png');
  expect(doc.twitter).toBe('newtwitter');
  expect(doc.facebook).toBe('newfacebook');
});

test(`test to list users`, async () => {
  let docs = await users_model.list();
  expect(Array.isArray(docs)).toBe(true);
});

test(`test to user password check for true`, async () => {
  let result = await users_model.userPasswordCheck('test user', 'new password');
  expect(result.check).toBe(true);
  expect(result.username).toBe('test user');
});

test(`test to user password check for false`, async () => {
  let result = await users_model.userPasswordCheck('test user', 'not new password');
  expect(result.check).toBe(false);
  expect(result.username).toBe('test user');
});

test(`test to destroy user`, async () => {
  await users_model.destroy('test user');
  let doc = await users_model.find('test user');
  expect(doc).toBeNull();
});