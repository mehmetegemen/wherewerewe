import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import _ from 'lodash'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  plugins: [
    createPersistedState()
  ],
  state: {
    token: null,
    user: null,
    isUserLoggedIn: false,
    messages: []
  },
  mutations: {
    setToken (state, token) {
      state.token = token
      state.isUserLoggedIn = !!(token)
    },
    setUser (state, user) {
      state.user = user
    },
    addMessage (state, message) {
      let messageWithTime = _.extend({}, message, {time: new Date().getTime()})
      state.messages.push(messageWithTime)
    },
    removeMessage (state) {
      state.messages.pop()
    }
  },
  actions: {
    setToken ({commit}, token) {
      commit('setToken', token)
    },
    setUser ({commit}, user) {
      commit('setUser', user)
    },
    addMessage ({commit}, message) {
      commit('addMessage', message)
    },
    removeMessage ({commit}) {
      commit('removeMessage')
    }
  }
})
