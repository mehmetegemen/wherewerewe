const letterAmount = 1000
export default (text) => {
  if (text.length > letterAmount) {
    let parsedText = text.substring(0, letterAmount)
    parsedText += '...'
    return parsedText
  }
  return text
}
