import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index/Index'
import Register from '@/components/Register.vue'
import Login from '@/components/Login'
import CreateNote from '@/components/Notes/Create'
import ViewNote from '@/components/Notes/View'
import MyNotes from '@/components/Notes/MyNotes'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'root',
      component: Index
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/createNote',
      name: 'create-note',
      component: CreateNote
    },
    {
      path: '/notes/:title',
      name: 'view-note',
      component: ViewNote
    },
    {
      path: '/myNotes',
      name: 'my-notes',
      component: MyNotes
    }
  ]
})
