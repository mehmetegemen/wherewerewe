import Api from './SocketApi'
import store from '@/store/store'
let socket = Api()
export default {
  connect (fn) {
    Api().on('connect', fn)
  },
  getNotification (callback) {
    socket.on('notification', function (data) {
      callback(data)
    })
  },
  initiateSocket () {
    socket.open()
  },
  closeSocket () {
    socket.close()
  },
  removeNotificationListener () {
    socket.removeAllListeners('notification')
  }
}
