import io from 'socket.io-client'
import store from '@/store/store'

export default () => {
  return io('http://localhost:3000/' + store.state.user._id, {
    query: {
      token: store.state.token
    }
  })
}
