import Api from './Api'

export default {
  create (data) {
    return Api().post('notes/create', data)
  },
  getNotes (options) {
    return Api().get('notes/getNotes/' + options.skip + '/' + options.limit)
  },
  findByTitle (title) {
    return Api().get('/notes/findByTitle/' + title)
  },
  listByAuthor (userId) {
    return Api().get('/notes/listByAuthor/' + userId)
  },
  delete (id) {
    return Api().get('notes/destroy/' + id)
  }
}
