import Api from './Api'

export default {
  create (data) {
    return Api().post('comments/create', data)
  },
  listByAuthor (author) {
    return Api().get('comments/listByAuthor/' + author)
  },
  listByPostId (postId) {
    return Api().get('comments/listByPostId/' + postId)
  },
  update (data) {
    return Api().post('comments/update', data)
  },
  destroy (commentId) {
    return Api().get('comments/destroy/' + commentId)
  },
  find (commentId) {
    return Api().get('comments/find/' + commentId)
  }
}
